function startGame() {
	var game = new Phaser.Game(BasicGame.width, BasicGame.height, Phaser.CANVAS, 'game');
	BasicGame.Preloader = function (game) {
		this.ready = false;
	};

	BasicGame.Preloader.prototype = {
		preload: function () {
			this.setGameConfig();
			this.game.load.image('debug', 'images/debug-grid-1920x1920.png');
			this.game.load.image('btn', 'images/btn.png');
			this.game.load.image('restartBtn', 'images/restart-btn.png');
			this.game.load.image('stopBtn', 'images/stop-btn.png');
			this.game.load.image('demoBtn', 'images/demo-btn.png');
			this.game.load.image('wall', 'images/wall.png');
			this.game.load.image('path', 'images/path.png');
			this.game.load.image('pathWin', 'images/pathWin.png');
			this.game.load.image('wallFinish', 'images/wallFinish.png');
			this.game.load.image('wallStart', 'images/wallStart.png');
			this.game.load.image('pathFinish', 'images/pathFinish.png');
			this.game.load.image('pathPreFinish', 'images/pathPreFinish.png');
			this.game.load.image('pathStart', 'images/pathStart.png');
			this.game.load.image('pathPreStart', 'images/pathPreStart.png');
			this.game.load.image('men', 'images/men4.png');
		},
		create: function () {
		},
		update: function () {
			this.ready = true;
			this.state.start('Game');
		},
		setGameConfig: function () {
			this.scale.setGameSize(BasicGame.width, BasicGame.height)
			this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
			this.scale.pageAlignVertically = true;
			this.scale.pageAlignHorizontally = true;
		}

	};
	BasicGame.Game = function (game) { };
	BasicGame.Game.prototype = {
		create: function () {
			let {
				mapWidth,
				mapHeight,
			} = BasicGame;
			this.game.physics.startSystem(Phaser.Physics.ARCADE);
			this.game.world.setBounds(0, 0, mapWidth, mapHeight + 34)
			data = JSON.parse(BasicGame.dataJSON);
			this.finishCoordinate = this.getFinCoord(data);
			this.gameStatus = true;
			this.demoStatus = false;
			this.numPosition = 0;
			this.groupWall = game.add.physicsGroup(Phaser.Physics.ARCADE);
			this.groupPath = game.add.physicsGroup(Phaser.Physics.ARCADE);
			this.groupWinPath = game.add.physicsGroup(Phaser.Physics.ARCADE);
			this.finishCell = game.add.sprite(this.finishCoordinate.x * 32, this.finishCoordinate.y * 32, 'pathFinish');
			this.game.physics.enable(this.finishCell, Phaser.Physics.ARCADE);
			this.men = this.add.sprite(38, 0, 'men');
			this.men.scale.setTo(0.7, 0.7)
			this.men.move = true;
			this.game.camera.follow(this.men, Phaser.Camera.FOLLOW_LOCKON, 0.1, 0.1);
			this.game.physics.enable(this.men, Phaser.Physics.ARCADE);
			this.cursors = this.game.input.keyboard.createCursorKeys();
			this.time = game.time.create(false);
			this.timeStatus = false;
			this.drawMaze(data);
			this.menuPanel();
		},
		menuPanel: function () {
			this.graphics = game.add.graphics(0, 0);
			this.graphics.beginFill('#000000')
			var menuProps = {
				width: BasicGame.cameraSize * 32,
				height: 34,
				x: 0,
				y: BasicGame.cameraSize * 32 - 34
			}
			this.menubar = this.graphics.drawRect(menuProps.x, menuProps.y, menuProps.width, menuProps.height)
			this.menubar.fixedToCamera = true;
			var demoBtnProps = {
				x: 5,
				y: BasicGame.height - 30
			}
			this.demoBtn = this.add.sprite(demoBtnProps.x, demoBtnProps.y, 'demoBtn');
			this.demoBtn.scale.setTo(0.4, 0.4)
			this.demoBtn.inputEnabled = true;
			this.demoBtn.input.useHandCursor = true;
			this.demoBtn.events.onInputUp.add(() => {
				if (!this.demoStatus) {
					this.drawWinPath();
					this.demoStatus = true;
					var men = this.men;
					men.x = 38;
					men.y = 0;
				}
			});
			this.demoBtn.fixedToCamera = true;

			var stopBtnProps = {
				x: 70,
				y: BasicGame.height - 30
			}
			this.stopBtn = this.add.sprite(stopBtnProps.x, stopBtnProps.y, 'stopBtn');
			this.stopBtn.scale.setTo(0.4, 0.4)
			this.stopBtn.inputEnabled = true;
			this.stopBtn.input.useHandCursor = true;
			this.stopBtn.events.onInputUp.add(() => {
				this.time.stop();
				this.men.move = false;
			})
			this.stopBtn.fixedToCamera = true;
			var restartBtnProps = {
				x: 135,
				y: BasicGame.height - 30
			}
			this.restartBtn = this.add.sprite(restartBtnProps.x, restartBtnProps.y, 'restartBtn');
			this.restartBtn.scale.setTo(0.4, 0.4)
			this.restartBtn.inputEnabled = true;
			this.restartBtn.input.useHandCursor = true;
			this.restartBtn.events.onInputUp.add(() => {
				this.men.x = 38;
				this.men.y = 0;
				this.timeStatus = false;
				this.timeText.text = `Time: 0s`;
				this.time.stop();
				this.men.move = true;
			})
			this.restartBtn.fixedToCamera = true;

			var btnProps = {
				x: 200,
				y: BasicGame.height - 30
			}
			this.btn = this.add.sprite(btnProps.x, btnProps.y, 'btn');
			this.btn.scale.setTo(0.4, 0.4)
			this.btn.inputEnabled = true;
			this.btn.input.useHandCursor = true;
			this.btn.events.onInputDown.add(function () {
				this.gameStatus = false;
				this.men.move = false;
				this.time.stop();
			}, this);
			this.btn.events.onInputUp.add(function () {
				this.drawWinPath();
			}, this);
			this.btn.fixedToCamera = true;

			var timeTextProps = {
				x: 265,
				y: BasicGame.height - 30
			}
			BasicGame.Game.prototype.timeText = game.add.text(timeTextProps.x, timeTextProps.y, '0s', {
				font: '20px Arial',
				fill: '#ffffff'
			});
			this.timeText.fixedToCamera = true;
		},
		demoRun: function () {
			var men = this.men;
			men.body.velocity.x = 200;
			men.body.velocity.y = 200;
			let path = this.calculateWinPath();
			if (path[this.numPosition] == undefined) {
				this.endRound()
			}
			let x = (path[this.numPosition][0] * 32) + 8;
			let y = (path[this.numPosition][1] * 32) + 8;
			this.game.physics.arcade.moveToXY(men, x, y, 200);
			let diffX = Math.abs(men.x - x);
			let diffY = Math.abs(men.y - y);
			if (diffX < 5
				&& diffY < 5) {
				this.numPosition++
			}
		},
		responseHandler: function (data) {
			let parseData = JSON.parse(data);
			parseData.forEach((i, id) => {
				parseData[id] = i.map(y => {
					if (+y === 1) {
						return 0
					} else {
						return 1
					}
				})
			})
			return parseData
		},
		update: function () {
			this.game.physics.arcade.collide(this.men, this.groupWall);
			this.game.physics.arcade.overlap(this.men, this.finishCell, this.endRound, null, this);
			this.men.body.velocity.x = 0;
			this.men.body.velocity.y = 0;
			if (this.cursors.left.isDown & this.men.move) {
				this.initTime();
				this.men.body.velocity.x = -200;
			} else if (this.cursors.right.isDown & this.men.move) {
				this.initTime();
				this.men.body.velocity.x = 200;
			}
			if (this.cursors.up.isDown & this.men.move) {
				this.initTime();
				this.men.body.velocity.y = -200;
			} else if (this.cursors.down.isDown & this.men.move) {
				this.initTime();
				this.men.body.velocity.y = 200;
			}
			if (this.game.input.mousePointer.isDown & this.men.move) {
				this.initTime();
				this.game.physics.arcade.moveToPointer(this.men, 200);

				if (Phaser.Rectangle.contains(this.men.body, this.game.input.x, this.game.input.y)) {
					this.men.body.velocity.setTo(0, 0);
				}
			}
			if (this.demoStatus) {
				this.demoRun();
			}
		},
		calculateWinPath: function () {
			let data = this.responseHandler(BasicGame.dataJSON)
			let grid = new PF.Grid(data);
			grid.setWalkableAt(0, 1, false);
			let finder = new PF.AStarFinder();
			let startCoordinate = {
				x: 1,
				y: 0
			};
			var path = finder.findPath(startCoordinate.x, startCoordinate.y, this.finishCoordinate.x, this.finishCoordinate.y, grid);
			return path
		},
		endRound: function () {
			if (this.gameStatus) {
				let curentTime = new Date();
				let finishTimeSecond = Math.floor((curentTime - this.timeStart) / 1000);
				let finishTimeMS = (curentTime - this.timeStart) % 1000;
				if (finishTimeMS < 100) {
					finishTimeMS = '0' + finishTimeMS;
				} else if (finishTimeMS < 10) {
					finishTimeMS = '00' + finishTimeMS;
				}
				let this_ = this;
				setTimeout(function () {
					this_.men.move = false;
				}, 500);
				this.gameStatus = false;

				this.time.stop();
				this.timeText.text =`Result: ${finishTimeSecond}.${finishTimeMS}s`;
			}
		},

		initTime: function () {
			if (!this.timeStatus) {
				this.timeStatus = true;
				this.timeStart = new Date();
				this.time.repeat(1 * Phaser.Timer.SECOND, 7200, this.updateTime, this);
				this.time.start();
			}
		},
		updateTime: function () {
			let time = new Date();
			let seconds = Math.round((time - this.timeStart) / 1000);
			let timeString = `${seconds}s`;

			this.timeText.text = timeString;
		},
		drawMaze: function (data) {
			let groupWall = this.groupWall;
			let groupPath = this.groupPath;
			data.forEach(function (item, i, ) {
				item.forEach(function (cell, indexWall) {

					switch (cell) {
						case "1":
							let pathSprite = groupPath.create(indexWall * 32, i * 32, 'path');
							pathSprite.body.immovable = true;
							break;
						case "0":
							let wallSprite = groupWall.create(indexWall * 32, i * 32, 'wall');
							wallSprite.body.immovable = true;
							break;
					}
				});
			});
		},
		drawWinPath: function () {
			let groupWinPath = this.groupWinPath;
			let path = this.calculateWinPath()
			path.forEach(coordinate => {
				let [x, y] = coordinate;
				let winPathSprite = groupWinPath.create(x * 32, y * 32, 'pathWin');
				winPathSprite.body.immovable = true;
			})
		},
		getFinCoord: function (data) {
			let dataLen = data.length;
			let itemLen = data[0].length;
			return {
				x: itemLen - 2,
				y: dataLen - 1
			}
		}

	};
	(function () {
		game.state.add('Preloader', BasicGame.Preloader);
		game.state.add('Game', BasicGame.Game);
		game.state.start('Preloader');
	})();
}
