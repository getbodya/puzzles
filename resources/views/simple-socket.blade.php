<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
    </head>
    <body>
        <button type="button" name="button" onclick="send()">send</button>

        <script>
            var conn = new WebSocket("ws://localhost:8082");
            conn.onopen = function(e) {
                console.log("Connection established!");
            };

            conn.onmessage = function() {
                console.log("Получены данные: " + e.data);
            };

            function send() {   
                var data = 'Данные для отправки: ' + Math.random();
                conn.send(data);
                console.log('Отправлено: ' + data);
            }
        </script>
    </body>
</html>
