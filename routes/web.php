<?php

Route::get('/', 'IndexController@index');

Route::get('/maze/{type}', 'PuzzlesController@generateMaze');

Route::get('init-event', function () {
    $data = [
        'topic_id' => 'onNewData',
        'data' => 'somData: ' . rand(0, 1000)
    ];

    \App\Services\Socket\PusherSocket::sentDataToServer($data);
});
